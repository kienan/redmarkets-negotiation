class Negotiation {

    constructor(room) {
        this.socket = io();
        this.socket.on('connect', function() {
            console.log("Trying to join room " + room);
            this.socket.emit('join negotiation', {"room": room});
        });
        window.addEventListener('beforeunload', function(e) {
            console.log("Trying to leave room " + room);
            negotiation.socket.emit('leave negotiation', {"room": room});
            negotiation.socket.disconnect();
            return true;
        });
        this.socket.on('participants changed', function(data) {
            console.log(data);
        });
    }
}

# Installlation

## Prerequisites

    apt install pipenv

## Dependency installation

    pipenv sync

## Running

    FLASK_ENV=development FLASK_APP=negotiation.py pipenv run ./negotiation.py

# License

AGPLv3+

WIP credits:

 * Banner image from https://benjaminhuen.blogspot.com/2010/12/spetch-dump.html
 * Icons from https://opengameart.org/content/700-rpg-icons
 * NordSudA font from https://fontlibrary.org/en/font/nord-sud
 * Theme modified from https://github.com/arulrajnet/attila
 * Coloured icons from https://opengameart.org/content/colored-ability-icons

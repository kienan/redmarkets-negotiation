FROM debian:buster
RUN apt-get update && apt-get install -y \
    pipenv virtualenv
RUN apt-get install -y wamerican
COPY Pipfile /app/
COPY Pipfile.lock /app/
COPY negotiation.py /app/
COPY static /app/static
COPY templates /app/templates
WORKDIR /app
ENV PIPENV_VENV_IN_PROJECT="enabled"
ENV APP_BINDADDRESS=0.0.0.0
RUN LANG=en_US.UTF-8 pipenv sync
ENTRYPOINT ["/usr/bin/pipenv", "run", "./negotiation.py"]

# to build: docker build -t redmarkets
# to run: docker run -p 5000:5000 redmarkets:latest
